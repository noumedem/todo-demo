class EditItem < HyperComponent
  param :todo
  triggers :save                             # add
  triggers :cancel                           # add
  after_mount { DOM[dom_node].focus }        # add

  render do
    INPUT(defaultValue: @Todo.title)
        .on(:enter) do |evt|
      @Todo.update(title: evt.target.value)
      save!                                  # add
    end
        .on(:blur) { cancel! }                   # add
  end
end
